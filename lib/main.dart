// ignore_for_file: library_private_types_in_public_api

import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:npp/Aboutuspage.dart';
import 'package:npp/helpscreen.dart';
import 'package:npp/signin.dart';
import 'package:npp/splshScreen.dart';
import 'package:pdf/pdf.dart';
import 'dart:math';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:csv/csv.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

List<String> _timeSlots = [
  '4PM-5PM',
  '3PM-4PM',
  '2PM-3PM',
  '1PM-2PM',
  '12PM-1PM',
  '11AM-12PM',
  '10AM-11AM',
  '9AM-10AM',
  '8AM-9AM',
];

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Generator',
      home: TimetableGenerator(),
    );
  }
}

class TimetableGenerator extends StatefulWidget {
  const TimetableGenerator({super.key});

  @override
  _TimetableGeneratorState createState() => _TimetableGeneratorState();
}

class _TimetableGeneratorState extends State<TimetableGenerator> {
  int _subjectCount = 0;
  final List<Subject> _subjects = [];
  final List<String> _days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
  final List<String> _rooms = [
    'Room1',
    'Room2',
    'Room3',
    'Room4',
    'Room5',
    'Room6',
    'Room7',
    'Room8',
    'Room9',
    'Room10',
    'Room11',
    'Room12',
    'Room13',
  ]; // Initialize as empty
  final List<String> _teachers = [
    'Dr.Sadaf Tanveer',
    'Dr. Amjad Khan',
    'Profeser. M.Hanan',
    'Profeser. M.Murtaza',
    'Dr. Yosaf Khan',
    'Dr. Naveen ',
    'Professor Aqsa Ashfaq',
    'Professor Madeha Hina',
    'Ms Faiza',
    'Dr. Jamal Hussein',
    'Professor Farida Ahmed',
    'Professor Nadiya Rahman'
  ]; // Initialize as empty
  int? hour;
  Timetable1? _bestTimetable;

  @override
  void initState() {
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();
  final _subjectCountController = TextEditingController();
  final List<TextEditingController> _subjectNameControllers = [];
  final List<TextEditingController> _creditHoursControllers = [];
  final _roomsController = TextEditingController(); // For room input
  final _teachersController = TextEditingController(); // For teacher input
  final _advancedDrawerController = AdvancedDrawerController();
  Subject? findSubject(Timetable1? timetable, String timeSlot, String day) {
    if (timetable == null) {
      return null; // Handle case where timetable is not yet generated
    }
    int dayIndex = _days.indexOf(day);
    int timeIndex = _timeSlots.indexOf(timeSlot);
    return timetable.schedule[timeIndex][dayIndex];
  }

  @override
  void dispose() {
    // Dispose of controllers to avoid memory leaks
    _subjectCountController.dispose();
    for (var controller in _subjectNameControllers) {
      controller.dispose();
    }
    for (var controller in _creditHoursControllers) {
      controller.dispose();
    }
    _roomsController.dispose();
    _teachersController.dispose();
    super.dispose();
  }

  // Widget _buildTimetableGrid() {
  //   return SingleChildScrollView(
  //     // Allow scrolling if timetable is long
  //     scrollDirection: Axis.horizontal,
  //     child: SingleChildScrollView(
  //       scrollDirection: Axis.vertical,
  //       child: DataTable(
  //         dataRowHeight: 150,
  //         columnSpacing: 10,
  //         horizontalMargin: 20,
  //         columns: [
  //           DataColumn(
  //               label:
  //                   SizedBox(width: 150, child: Text('Time'))), // Time column
  //           ..._days.map((day) => DataColumn(label: Text(day))), // Day headers
  //         ],
  //         rows: _timeSlots.map((timeSlot) {
  //           return DataRow(cells: [
  //             DataCell(Text(timeSlot)), // Time slot cell
  //             ..._days.map((day) {
  //               Subject? subject = findSubject(_bestTimetable, timeSlot, day);
  //               bool isBreakTime = timeSlot == '1PM-2PM';
  //               return DataCell(
  //                 subject != null
  //                     ? Container(
  //                         decoration: BoxDecoration(
  //                           border: Border.all(color: Colors.grey[300]!),
  //                           color: subject != null
  //                               ? isBreakTime
  //                                   ? Colors.green
  //                                   : Colors.primaries[Random()
  //                                           .nextInt(Colors.primaries.length)]
  //                                       .withOpacity(0.3)
  //                               : null,
  //                         ),
  //                         width: 150,
  //                         child: isBreakTime
  //                             ? const Text('Break Time')
  //                             : Center(
  //                                 child: Column(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                   children: [
  //                                     Text(subject.name), // Subject name
  //                                     if (subject.room != null)
  //                                       Text(subject.room!), // Room
  //                                     if (subject.teacher != null)
  //                                       Text(subject.teacher!), // Teacher
  //                                   ],
  //                                 ),
  //                               ),
  //                       )
  //                     : Container(), // Empty cell if no subject
  //               );
  //             }).toList(),
  //           ]);
  //         }).toList(),
  //       ),
  //     ),
  //   );
  // }
  Widget _buildTimetableGrid() {
    return SingleChildScrollView(
      // Allow scrolling if timetable is long
      scrollDirection: Axis.horizontal,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: DataTable(
          dataRowHeight: 150,
          columnSpacing: 10,
          horizontalMargin: 20,
          columns: [
            const DataColumn(
                label:
                    SizedBox(width: 150, child: Text('Time'))), // Time column
            ..._days.map((day) => DataColumn(label: Text(day))), // Day headers
          ],
          rows: _timeSlots.reversed.toList().asMap().entries.map((entry) {
            int timeIndex = entry.key;
            String timeSlot = entry.value;

            return DataRow(cells: [
              DataCell(Text(timeSlot)), // Time slot cell
              ..._days.map((day) {
                Subject? subject = findSubject(_bestTimetable, timeSlot, day);
                bool isBreakTime = timeSlot == '1PM-2PM';

                return DataCell(
                  subject != null
                      ? Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey[300]!),
                            color: subject.name != 'Free time'
                                ? isBreakTime
                                    ? Colors.green
                                    : Colors.primaries[Random()
                                            .nextInt(Colors.primaries.length)]
                                        .withOpacity(0.3)
                                : null,
                          ),
                          width: 150,
                          child: isBreakTime
                              ? const Text('Break Time')
                              : Center(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(subject.name), // Subject name
                                      if (subject.room != null)
                                        Text(subject.room!), // Room
                                      if (subject.teacher != null)
                                        Text(subject.teacher!), // Teacher
                                    ],
                                  ),
                                ),
                        )
                      : Container(), // Empty cell if no subject
                );
              }).toList(),
            ]);
          }).toList(),
        ),
      ),
    );
  }
  // Widget _buildTimetableRows() {
  //   return Column(
  //     children: _timeSlots.map(
  //       (timeSlot) {
  //         bool isBreakTime = timeSlot == '1PM-2PM'; // Check for break time
  //         return SingleChildScrollView(
  //           scrollDirection: Axis.horizontal,
  //           child: Row(
  //             children: [
  //               Container(
  //                 height: 120,
  //                 width: MediaQuery.of(context).size.width / _days.length,
  //                 alignment: Alignment.center, // Align time to center
  //                 child: Text(timeSlot,
  //                     style: const TextStyle(fontWeight: FontWeight.bold)),
  //               ),
  //               ..._days.map(
  //                 (day) {
  //                   Subject? subject =
  //                       findSubject(_bestTimetable, timeSlot, day);
  //                   return Container(
  //                     height: 120,
  //                     // width: MediaQuery.of(context).size.width / _days.length,
  //                     child: Expanded(
  //                       child: Container(
  //                         height: 120,
  //                         // width:
  //                         // MediaQuery.of(context).size.width / _days.length,
  //                         decoration: BoxDecoration(
  //                           border: Border.all(color: Colors.grey[300]!),
  //                           color: subject != null
  //                               ? isBreakTime
  //                                   ? Colors.green
  //                                   : Colors.primaries[Random()
  //                                           .nextInt(Colors.primaries.length)]
  //                                       .withOpacity(0.3)
  //                               : null,
  //                         ),
  //                         padding: const EdgeInsets.all(8.0),
  //                         child: subject != null
  //                             ? isBreakTime
  //                                 ? const Text('Break Time')
  //                                 : Column(
  //                                     children: [
  //                                       Text(subject.name,
  //                                           style: TextStyle(
  //                                               fontWeight: FontWeight.bold,
  //                                               color: subject != null
  //                                                   ? Colors.black
  //                                                   : null)),
  //                                       if (subject.room != null)
  //                                         Text('${subject.room}',
  //                                             style: TextStyle(
  //                                                 fontSize: 12,
  //                                                 color: subject != null
  //                                                     ? Colors.black
  //                                                     : null)),
  //                                       if (subject.teacher != null)
  //                                         Text('${subject.teacher}',
  //                                             style: TextStyle(
  //                                                 fontSize: 12,
  //                                                 color: subject != null
  //                                                     ? Colors.black
  //                                                     : null)),
  //                                     ],
  //                                   )
  //                             : null, // Empty container if no subject
  //                       ),
  //                     ),
  //                   );
  //                 },
  //               ).toList(),
  //             ],
  //           ),
  //         );
  //       },
  //     ).toList(),
  //   );
  // }

  void _showDownloadOptions(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.picture_as_pdf),
                title: const Text('Download as PDF'),
                onTap: _downloadAsPdf,
              ),
              ListTile(
                leading: const Icon(Icons.table_chart),
                title: const Text('Download as CSV'),
                onTap: _downloadAsCsv,
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _downloadAsPdf() async {
    if (await Permission.storage.request().isGranted) {
      final pdf = pw.Document();

      pdf.addPage(pw.Page(
        build: (pw.Context context) {
          return pw.Table(
            border: pw.TableBorder.all(
              width: 1,
            ), // Add borders to match DataTable
            columnWidths: {
              // Set fixed width for time column
              0: pw.FixedColumnWidth(150),
            },
            children: [
              // Time Slot Headers Row (Reversed like in DataTable)
              pw.TableRow(
                children: [
                  pw.Container(width: 150, child: pw.Text('Time')),
                  ..._days.map((day) =>
                      pw.Expanded(child: pw.Center(child: pw.Text(day)))),
                ],
              ),

              // Data Rows (Reversed and mapped like in DataTable)
              ..._timeSlots.reversed.toList().asMap().entries.map((entry) {
                int timeIndex = entry.key;
                String timeSlot = entry.value;

                return pw.TableRow(
                  children: [
                    pw.Container(width: 150, child: pw.Text(timeSlot)),
                    ..._days.map((day) {
                      Subject? subject =
                          findSubject(_bestTimetable, timeSlot, day);
                      bool isBreakTime = timeSlot == '1PM-2PM';

                      // Cell styling to mimic the DataTable appearance
                      return pw.Expanded(
                        child: pw.Container(
                          decoration: pw.BoxDecoration(
                            border: pw.Border.all(color: PdfColors.grey300),
                            color: subject != null
                                ? isBreakTime
                                    ? PdfColors
                                        .green200 // Light green for break time
                                    : PdfColors
                                        .white // Random pastel primary color
                                : null,
                          ),
                          padding: const pw.EdgeInsets.all(
                              8), // Add padding to match DataTable cells
                          child: isBreakTime
                              ? pw.Center(child: pw.Text('Break Time'))
                              : subject != null
                                  ? pw.Column(
                                      crossAxisAlignment:
                                          pw.CrossAxisAlignment.start,
                                      children: [
                                        pw.Text(subject.name),
                                        if (subject.room != null)
                                          pw.Text(subject.room!),
                                        if (subject.teacher != null)
                                          pw.Text(subject.teacher!),
                                      ],
                                    )
                                  : pw.Container(), // Empty cell
                        ),
                      );
                    }).toList(),
                  ],
                );
              }).toList(),
            ],
          );
        },
      ));

      // ... (rest of the PDF saving and sharing code is the same)
      final output = await getTemporaryDirectory();
      final file = File('${output.path}/TimeTable.pdf');
      await file.writeAsBytes(await pdf.save());
      print(file);
      await Printing.sharePdf(
          bytes: await pdf.save(), filename: 'TimeTable.pdf');
    }
  }

  Future<void> _downloadAsCsv() async {
    if (await Permission.storage.request().isGranted) {
      // CSV data preparation, mimicking the _buildTimetableGrid logic:

      List<List<dynamic>> rows = [
        ['Time', ..._days], // Header row (Time, Day1, Day2, ...)
        ..._timeSlots.reversed.toList().asMap().entries.map((entry) {
          int timeIndex = entry.key;
          String timeSlot = entry.value;

          return [
            timeSlot, // Time slot in the first column
            ..._days.map((day) {
              Subject? subject = findSubject(_bestTimetable, timeSlot, day);
              bool isBreakTime = timeSlot == '1PM-2PM';

              // Determine cell content similar to _buildTimetableGrid
              return isBreakTime
                  ? 'Break Time'
                  : subject != null
                      ? '${subject.name}\n${subject.room ?? ''}\n${subject.teacher ?? ''}' // Combine subject details if available
                      : '-'; // Empty cell
            }).toList(),
          ];
        }).toList(),
      ];

      // CSV conversion and saving (remains the same):

      String csv = const ListToCsvConverter().convert(rows);
      final directory = await getApplicationDocumentsDirectory();
      final path = '${directory.path}/TimeTable.csv';
      final file = File(path);
      await file.writeAsString(csv);

      // (Optionally uncomment to share the CSV file)
      // Share.shareFiles([path], text: 'Here is your timetable!');
    }
  }

  void _handleMenuButtonPressed() {
    // NOTICE: Manage Advanced Drawer state through the Controller.
    // _advancedDrawerController.value = AdvancedDrawerValue.visible();
    _advancedDrawerController.showDrawer();
  }


/////////////////////////DRAWER///////////////////////
  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
      backdrop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.green, Colors.lightGreen.withOpacity(0.2)],
          ),
        ),
      ),
      controller: _advancedDrawerController,
      animationCurve: Curves.easeInOut,
      animationDuration: const Duration(milliseconds: 300),
      animateChildDecoration: true,
      rtlOpening: false,
      // openScale: 1.0,
      disabledGestures: false,
      childDecoration: const BoxDecoration(
        // NOTICE: Uncomment if you want to add shadow behind the page.
        // Keep in mind that it may cause animation jerks.
        // boxShadow: <BoxShadow>[
        //   BoxShadow(
        //     color: Colors.black12,
        //     blurRadius: 0.0,
        //   ),
        // ],
        borderRadius: const BorderRadius.all(Radius.circular(16)),
      ),
      drawer: SafeArea(
        child: Container(
          child: ListTileTheme(
            textColor: Colors.black,
          //  iconColor: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: 158.0,
                  height: 158.0,
                  margin: const EdgeInsets.only(
                    top: 30.0,
                    bottom: 40.0,
                  ),
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.green, width: 7.0),
                  ),
                  child: ClipOval(
                    child: Image.asset('images/SMT.jpeg', fit: BoxFit.cover),
                  ),
                ),


                ListTile(
                  onTap: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => TimetableGenerator()),
                    );
                  },
                  leading: const Icon(Icons.home, color: Colors.green),
                  title: const Text('Home', style: TextStyle(color: Colors.black)),
                ),
                Divider(
                  color: Colors.black,
                  thickness: 1.0,
                  height: 20.0,
                  indent: 16.0,
                  endIndent: 16.0,
                ),

                ListTile(
                  onTap: () {
                    Get.to(() => HelpScreen());
                  },
                  leading: const Icon(Icons.account_circle_rounded, color: Colors.blue),
                  title: const Text('Help', style: TextStyle(color: Colors.black)),
                ),
                Divider(
                  color: Colors.black,
                  thickness: 1.0,
                  height: 20.0,
                  indent: 16.0,
                  endIndent: 16.0,
                )
,
                ListTile(
                  onTap: () {
                    Get.to(() => AboutUsPage());
                  },
                  leading: const Icon(Icons.favorite, color: Colors.black),
                  title: const Text('About', style: TextStyle(color: Colors.black)),
                ),
                Divider(
                  color: Colors.black,
                  thickness: 1.0,
                  height: 20.0,
                  indent: 20.0,
                  endIndent: 10.0,
                )
,
                ListTile(
                  onTap: () {
                    var auth = FirebaseAuth.instance;
                    auth.signOut().then((value) {
                      Get.offAll(() => LoginPage());
                    });
                  },
                  leading: const Icon(Icons.logout, color: Colors.red),
                  title: const Text('Logout', style: TextStyle(color: Colors.black)),

                ),
                const Spacer(),
                DefaultTextStyle(
                  style: const TextStyle(
                    fontSize: 12,
                    color: Colors.white54,
                  ),
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 16.0,
                    ),
                    child: const Text(
                      'Terms of Service || Privacy Policy',
                      style: TextStyle(color: Colors.black),
                    ),

                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: _handleMenuButtonPressed,
            icon: ValueListenableBuilder<AdvancedDrawerValue>(
              valueListenable: _advancedDrawerController,
              builder: (_, value, __) {
                return AnimatedSwitcher(
                  duration: Duration(milliseconds: 250),
                  child: Icon(
                    value.visible ? Icons.clear : Icons.menu,
                    key: ValueKey<bool>(value.visible),
                  ),
                );
              },
            ),
          ),
          title: const Text(' Generator'),
          actions: [
            if (_bestTimetable != null)
              IconButton(
                icon: const Icon(Icons.download),
                onPressed: () => _showDownloadOptions(context),
              ),
            IconButton(
              icon: const Icon(Icons.add),
              onPressed: () {
                setState(() {
                  addLab(_bestTimetable!);
                });
              },
            ),
          ],
        ),
        ////////////////////DRAWER end////////////////////



        body: SingleChildScrollView(
          child: Column(children: [
            if (_bestTimetable != null) _buildTimetableGrid(),
            if (_bestTimetable == null) ...[
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _subjectCountController,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                          labelText: 'Number of Subjects'),
                      validator: (value) {
                        int? count = int.tryParse(value!);
                        if (value == null || value.isEmpty) {
                          return 'Please enter a number';
                        } else if (count == null || count <= 4) {
                          return 'Please enter a valid number greater than 5';
                        }
                        return null;
                      },
                      onChanged: (value) {
                        if (_formKey.currentState!.validate()) {
                          _updateSubjectInputFields();
                        }
                      },
                    ),
                    const SizedBox(height: 15),
                    Builder(builder: (context) {
                      if (_subjectCount == 0) {
                        return Container();
                      }
                      return SizedBox(
                        height: 400,
                        child: ListView(
                          children: List.generate(_subjectCount,
                              (index) => _subjectInputRow(index + 1)),
                        ),
                      );
                    }),
                  ],
                ),
              ),
              SizedBox(
                height: 50, // Adjusted height
                width: 140, // Adjusted width
                child: ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      _collectSubjectData();
                      _bestTimetable = await generateTimetables(_subjects);
                      setState(() {
                        adjustSubjectOccurrences(_bestTimetable, _subjects);
                        for (int i = 0; i < _timeSlots.length; i++) {
                          String day = _timeSlots[i];
                          List<Subject?> subjects = _bestTimetable!.schedule[i];
                          print("$day: ${subjects.map((s) => s?.name ?? 'Free').join(', ')}");
                        }
                      });
                    }
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.green), // Gradient color
                    elevation: MaterialStateProperty.all<double>(10), // Remove elevation
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0), // Adjusted border radius
                      ),
                    ),
                  ),
                  child: const Text('Generate', style: TextStyle(color: Colors.black)), // Text color
                ),
              ),

            ]
          ]),
        ),
      ),
    );
  }

  Timetable1 adjustSubjectOccurrences(
      Timetable1? bestTimetable, List<Subject> subjects) {
    if (bestTimetable == null) {
      throw ArgumentError("Best timetable cannot be null.");
    }

    // Define a dummy subject
    final dummySubject =
        Subject(name: "Free time", creditHours: 0, room: '', teacher: '');

    // Count subject occurrences in the best timetable
    final subjectCounts = <Subject, int>{};
    for (var day in bestTimetable.schedule) {
      for (var subject in day) {
        if (subject != null && subject != dummySubject) {
          subjectCounts[subject] = (subjectCounts[subject] ?? 0) + 1;
        }
      }
    }

    // Replace excess occurrences with the dummy subject
    for (var subject in subjects) {
      int occurrences = subjectCounts[subject] ?? 0;
      if (occurrences > subject.creditHours) {
        for (var day in bestTimetable.schedule) {
          for (int i = 0; i < day.length; i++) {
            if (day[i] == subject) {
              day[i] = dummySubject;
              occurrences--;
              if (occurrences == subject.creditHours) break;
            }
          }
          if (occurrences == subject.creditHours) break;
        }
      }
    }

    return bestTimetable; // Return the modified timetable
  }

  Timetable1 addLab(Timetable1 bestTimetable) {
    final dummySubject =
        Subject(name: "Free time", creditHours: 0, room: '', teacher: '');
    final labSubject = Subject(
        name: "Lab", creditHours: 2, room: 'Lab TimetableGenerator', teacher: 'Lab Teacher');

    for (int i = 0; i < bestTimetable.schedule[0].length; i++) {
      bool labAdded = false;

      for (int j = 0; j < bestTimetable.schedule.length - 1; j++) {
        if (bestTimetable.schedule[j][i]!.name == dummySubject.name &&
            bestTimetable.schedule[j + 1][i]!.name == dummySubject.name) {
          bestTimetable.schedule[j][i] = labSubject;
          bestTimetable.schedule[j + 1][i] = labSubject;
          labAdded = true;
          break; // Exit inner loop once lab is added
        }
      }

      if (labAdded) {
        break; // Exit outer loop once lab is added
      }
    }

    return bestTimetable;
  }

  void _updateSubjectInputFields() {
    setState(() {
      _subjectCount = int.parse(_subjectCountController.text);
      _subjectNameControllers.clear();
      _creditHoursControllers.clear();
      for (int i = 0; i < _subjectCount; i++) {
        _subjectNameControllers.add(TextEditingController());
        _creditHoursControllers.add(TextEditingController());
      }
    });
  }

  Widget _subjectInputRow(int index) {
    return Row(
      children: [
        Text('Subject $index:'),
        const SizedBox(width: 10),
        Expanded(
            child: TextFormField(
                controller: _subjectNameControllers[index - 1],
                decoration: const InputDecoration(labelText: 'Name'),
                validator: (value) {
                  return null;
                })),
        const SizedBox(width: 10),
        Expanded(
            child: TextFormField(
          controller: _creditHoursControllers[index - 1],
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(labelText: 'Credit Hours'),
          validator: (value) {
            return null;
          },
        )),
      ],
    );
  }

  void _collectSubjectData() {
    _subjects.clear();
    for (int i = 0; i < _subjectCount; i++) {
      String name = _subjectNameControllers[i].text;
      int creditHours = int.parse(_creditHoursControllers[i].text);
      String room = _rooms[i % _rooms.length];
      String teacher = _teachers[i % _teachers.length];
      _subjects.add(Subject(
          name: name, creditHours: creditHours, room: room, teacher: teacher));
    }
  }

  Future<Timetable1> generateTimetables(List<Subject> allSubjects) async {
    int populationSize = 100;
    int maxGenerations = 100;
    double mutationRate = 0.1;
    int tournamentSize = 3;
    Timetable1 bestTimetableSoFar = Timetable1(
        List.generate(
            _timeSlots.length, (_) => List.filled(_days.length, null)),
        -1000);
    List<Timetable1> population =
        _initializePopulation(allSubjects, populationSize);
    for (int generation = 0; generation < maxGenerations; generation++) {
      List<int> fitnessScores = _evaluatePopulation(population, allSubjects);
      List<Timetable1> selectedParents =
          _tournamentSelection(population, fitnessScores, tournamentSize);
      List<Timetable1> offspring =
          _crossoverAndMutate(selectedParents, mutationRate);
      population = offspring;

      bestTimetableSoFar = _updateBestTimetable(bestTimetableSoFar, population);
      if (generation == maxGenerations - 1) {}
    }

    return bestTimetableSoFar;
  }

  List<Timetable1> _initializePopulation(List<Subject> subjects, int size) {
    return List.generate(size, (_) => createRandomTimetable(subjects));
  }

  List<int> _evaluatePopulation(
      List<Timetable1> population, List<Subject> subjects) {
    return population
        .map((timetable) => calculateFitness(timetable, subjects))
        .toList();
  }

  List<Timetable1> _tournamentSelection(List<Timetable1> population,
      List<int> fitnessScores, int tournamentSize) {
    List<Timetable1> selectedParents = [];
    for (int i = 0; i < population.length; i++) {
      List<Timetable1> participants = List.generate(tournamentSize,
          (_) => population[Random().nextInt(population.length)]);
      double totalFitness = participants.fold<double>(
          0, (sum, p) => sum + fitnessScores[population.indexOf(p)]);
      List<double> probabilities = participants
          .map((p) => fitnessScores[population.indexOf(p)] / totalFitness)
          .toList();
      double randomValue = Random().nextDouble();
      double cumulativeProb = 0.0;
      for (int i = 0; i < tournamentSize; i++) {
        cumulativeProb += probabilities[i];
        if (randomValue <= cumulativeProb) {
          selectedParents.add(participants[i]);
          break;
        }
      }
    }

    return selectedParents;
  }

  List<Timetable1> _crossoverAndMutate(
      List<Timetable1> parents, double mutationRate) {
    List<Timetable1> offspring = [];
    for (int i = 0; i < parents.length / 2; i++) {
      List<Timetable1> children = crossover(parents[2 * i], parents[2 * i + 1]);
      offspring.add(mutate(children[0], mutationRate));
      offspring.add(mutate(children[1], mutationRate));
    }
    return offspring;
  }

  Timetable1 _updateBestTimetable(
      Timetable1 currentBest, List<Timetable1> population) {
    for (Timetable1 timetable in population) {
      if (timetable.fitness > currentBest.fitness) {
        currentBest = timetable.deepCopy();
      }
    }
    return currentBest;
  }

  int calculateFitness(Timetable1 timetable, List<Subject> allSubjects) {
    int fitness = 0;

    // Credit hour fulfillment
    // for (Subject subject in allSubjects) {
    //   int subjectCount = timetable.schedule.fold(
    //       0,
    //       (count, timeslot) =>
    //           timeslot
    //               .where((slotSubject) => slotSubject?.name == subject.name)
    //               .length +
    //           count);
    //   fitness += min(subjectCount, subject.creditHours);
    // }

    // Room conflicts
    for (var timeslot in timetable.schedule) {
      Set<String> assignedRooms = {}; // Rooms used in a particular timeslot
      for (var subject in timeslot) {
        if (subject?.room != null && assignedRooms.contains(subject!.room)) {
          fitness -= 5; // Penalty for room conflicts
        } else if (subject?.room != null) {
          assignedRooms.add(subject!.room!);
        }
      }
    }

    // Teacher Conflicts
    for (var timeslot in timetable.schedule) {
      Set<String> assignedTeachers =
          {}; // Teachers used in a particular timeslot
      for (var subject in timeslot) {
        if (subject?.teacher != null &&
            assignedTeachers.contains(subject!.teacher)) {
          fitness -= 5; // Penalty for teacher conflicts
        } else if (subject?.teacher != null) {
          assignedTeachers.add(subject!.teacher!);
        }
      }
    }

    return fitness;
  }

// Crossover Function
  List<Timetable1> crossover(Timetable1 timetable1, Timetable1 timetable2) {
    int cutPoint = Random().nextInt(_timeSlots.length);
    List<List<Subject?>> offspring1Schedule =
        timetable1.schedule.sublist(0, cutPoint) +
            timetable2.schedule.sublist(cutPoint);
    List<List<Subject?>> offspring2Schedule =
        timetable2.schedule.sublist(0, cutPoint) +
            timetable1.schedule.sublist(cutPoint);
    return [
      Timetable1(offspring1Schedule, 0),
      Timetable1(offspring2Schedule, 0)
    ];
  }

  Timetable1 mutate(Timetable1 timetable, double mutationRate) {
    Timetable1 mutatedTimetable = timetable.deepCopy();
    for (int timeslotIndex = 0;
        timeslotIndex < _timeSlots.length;
        timeslotIndex++) {
      if (_timeSlots[timeslotIndex] == '1PM-2PM')
        continue; // Skip the break time slot

      for (int dayIndex = 0; dayIndex < _days.length; dayIndex++) {
        if (Random().nextDouble() < mutationRate) {
          int pos1Day = Random().nextInt(_days.length - 1);
          int pos1Slot = Random().nextInt(_timeSlots.length - 1);
          if (_timeSlots[pos1Slot] == '1PM-2PM')
            continue; // Skip if the random position is the break time slot

          Subject? temp = mutatedTimetable.schedule[timeslotIndex][dayIndex];
          mutatedTimetable.schedule[timeslotIndex][dayIndex] =
              mutatedTimetable.schedule[pos1Slot][pos1Day];
          mutatedTimetable.schedule[pos1Slot][pos1Day] = temp;
        }
      }
    }

    return mutatedTimetable;
  }
// Mutation Function
  // Timetable1 mutate(Timetable1 timetable, double mutationRate) {
  //   Timetable1 mutatedTimetable = timetable.deepCopy();
  //   for (int timeslotIndex = 0;
  //       timeslotIndex < _timeSlots.length;
  //       timeslotIndex++) {
  //     for (int dayIndex = 0; dayIndex < _days.length; dayIndex++) {
  //       if (Random().nextDouble() < mutationRate) {
  //         int pos1Day = Random().nextInt(_days.length - 2);
  //         int pos1Slot = Random().nextInt(_timeSlots.length - 2);
  //         Subject? temp = mutatedTimetable.schedule[timeslotIndex][dayIndex];
  //         mutatedTimetable.schedule[timeslotIndex][dayIndex] =
  //             mutatedTimetable.schedule[pos1Slot][pos1Day];
  //         mutatedTimetable.schedule[pos1Slot][pos1Day] = temp;
  //       }
  //     }
  //   }

  //   return mutatedTimetable;
  // }

  List<Timetable1> tournamentSelection(
      List<Timetable1> population, List<int> fitnessScores) {
    int tournamentSize = 3;
    List<Timetable1> selectedParents = [];
    for (int i = 0; i < population.length; i++) {
      List<int> participants = [];
      while (participants.length < tournamentSize) {
        int randomIndex = Random().nextInt(population.length);
        if (!participants.contains(randomIndex)) participants.add(randomIndex);
      }

      // Find the best among them
      int bestIndex = participants[0];
      int bestFitness = fitnessScores[bestIndex];
      for (int index in participants) {
        if (fitnessScores[index] > bestFitness) {
          bestFitness = fitnessScores[index];
          bestIndex = index;
        }
      }

      selectedParents.add(population[bestIndex]);
    }

    return selectedParents;
  }

  Timetable1 createRandomTimetable(List<Subject> allSubjects) {
    List<List<Subject?>> schedule = List.generate(
        _timeSlots.length, (_) => List.filled(_days.length, null));
    for (int timeslotIndex = 0;
        timeslotIndex < _timeSlots.length;
        timeslotIndex++) {
      for (int dayIndex = 0; dayIndex < _days.length; dayIndex++) {
        if (_timeSlots[timeslotIndex] != '1PM-2PM') {
          schedule[timeslotIndex][dayIndex] =
              allSubjects[Random().nextInt(allSubjects.length)];
        } else {
          schedule[timeslotIndex][dayIndex] = Subject(
              name: 'Break Time', creditHours: 0, room: '', teacher: '');
        }
      }
    }
    return Timetable1(schedule, 0);
  }
  // Timetable1 createRandomTimetable(List<Subject> allSubjects) {
  //   List<List<Subject?>> schedule = List.generate(
  //       _timeSlots.length, (_) => List.filled(_days.length, null));
  //   for (int timeslotIndex = 0;
  //       timeslotIndex < _timeSlots.length;
  //       timeslotIndex++) {
  //     for (int dayIndex = 0; dayIndex < _days.length; dayIndex++) {
  //       schedule[timeslotIndex][dayIndex] =
  //           allSubjects[Random().nextInt(allSubjects.length)];
  //     }
  //   }
  //   return Timetable1(schedule, 0);
  // }
}

class Subject {
  String name;
  int creditHours;
  String? room;
  String? teacher;

  Subject(
      {required this.name, required this.creditHours, this.room, this.teacher});
}

class Timetable1 {
  List<List<Subject?>> schedule;
  int fitness;

  Timetable1(this.schedule, this.fitness);

  Timetable1 deepCopy() {
    List<List<Subject?>> newSchedule =
        List.generate(_timeSlots.length, (i) => List.from(schedule[i]));
    return Timetable1(newSchedule, fitness);
  }
}
