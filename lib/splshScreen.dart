import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:npp/main.dart';
import 'dart:async';
import 'package:npp/signin.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    );
    _controller.repeat(reverse: true);

    FirebaseAuth auth = FirebaseAuth.instance;
    User? currentUser = auth.currentUser; // Get current user

    Timer(
      const Duration(seconds: 5), // Adjust as needed
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => currentUser != null
                ? const TimetableGenerator() // User logged in, go to dashboard
                : const LoginPage() // User not logged in, go to sign-in
            ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedBuilder(
              animation: _animation,
              builder: (context, child) {
                return Transform.translate(
                  offset: Offset(0.0, 20.0 * _animation.value),
                  child: child,
                );
              },
              child: Container(
                width: 250 + 9 * 4, // Account for border width
                height: 250 + 9 * 5, // Account for border width
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.green, width: 9),
                ),
                child: ClipOval(
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: ClipOval(
                      child: Image.asset('images/SMT.jpeg'),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            FadeTransition(
              opacity: _animation,
              child: const Text(
                'Welcome to Smart Timetable',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
