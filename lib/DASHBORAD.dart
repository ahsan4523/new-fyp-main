import 'package:flutter/material.dart';

class Subject {
  final String name;
  final int creditHours;

  Subject(this.name, this.creditHours);
}

class SubjectInput extends StatefulWidget {
  final Function(Subject) onSubjectAdded;

  const SubjectInput({super.key, required this.onSubjectAdded});

  @override
  State<SubjectInput> createState() => _SubjectInputState();
}

class _SubjectInputState extends State<SubjectInput> {
  final _nameController = TextEditingController();
  final _creditHoursController = TextEditingController();

  void _addSubject() {
    final name = _nameController.text;
    final creditHours =
        int.tryParse(_creditHoursController.text) ?? 0; // Handle invalid input
    if (name.isNotEmpty) {
      widget.onSubjectAdded(Subject(name, creditHours));
      _nameController.clear();
      _creditHoursController.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            controller: _nameController,
            decoration: const InputDecoration(labelText: 'Subject Name'),
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: TextField(
            controller: _creditHoursController,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(labelText: 'Credit Hours'),
          ),
        ),
        IconButton(
          icon: const Icon(Icons.add),
          onPressed: _addSubject,
        ),
      ],
    );
  }
}

List<List<TimetableEntry>> generateTimetable(List<Subject> subjects) {
  final predefinedRooms = [
    // Define your list of predefined rooms here
    'Room A',
    'Room B',
    'Room C',
    // ... Add more rooms as needed
  ];

  // Implement your timetable generation algorithm here
  // This is a complex process and might require libraries like `scheduling` (https://pub.dev/packages/schedulers)
  // for advanced scheduling techniques.
  // Here's a basic example (replace with your actual logic):
  final timetable = List.generate(
      5,
      (_) => List.generate(
          9, (_) => TimetableEntry())); // 5 days, 9 slots (8:30am-5:30pm)
  int slot = 0;
  for (final subject in subjects) {
    for (int day = 0; day < 5; day++) {
      if (slot + subject.creditHours <= 9) {
        for (int i = 0; i < subject.creditHours; i++) {
          timetable[day][slot + i] = TimetableEntry(
              subject: subject,
              room: predefinedRooms[i % predefinedRooms.length]);
        }
        slot += subject.creditHours;
      } else {
        break; // Handle cases where a subject cannot fit in the remaining slots for a day
      }
    }
  }
  return timetable;
}

class TimetableEntry {
  final Subject? subject;
  final String room;

  TimetableEntry({this.subject, this.room = ''});
}

class TimetableScreen extends StatelessWidget {
  final List<List<TimetableEntry>> timetable;

  const TimetableScreen({super.key, required this.timetable});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Timetable'),
        actions: [
          IconButton(icon: const Icon(Icons.download), onPressed: () {}
              // _downloadTimetable(context), // Function to download PDF
              ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Text('Timetable', style: TextStyle(fontSize: 20)),
            Table(
              border: TableBorder.all(color: Colors.grey),
              children: [
                TableRow(
                  children: [
                    const TableCell(child: Text('Day')),
                    ...List.generate(
                        9, (index) => Text(getTimeSlot(index))),
                  ],
                ),
                ...List.generate(5, (dayIndex) => _buildTimetableRow(dayIndex)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String getTimeSlot(int index) {
    final startTime = Duration(hours: 8, minutes: 30 + index * 30);
    final endTime = startTime + const Duration(minutes: 30);
    return '${startTime.toString().substring(11, 16)} - ${endTime.toString().substring(11, 16)}';
  }

  TableRow _buildTimetableRow(int dayIndex) {
    return TableRow(
      children: [
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Text(weekdays[dayIndex]),
        ),
        ...List.generate(
            9,
            (slotIndex) =>
                _buildTimetableEntry(timetable[dayIndex][slotIndex])),
      ],
    );
  }

  Widget _buildTimetableEntry(TimetableEntry entry) {
    if (entry.subject != null) {
      return TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Column(
          children: [
            Text(entry.subject!.name),
            Text('(${entry.subject!.creditHours} CH)'),
            Text(entry.room),
          ],
        ),
      );
    } else {
      return const TableCell(child: Text(''));
    }
  }

  // void _downloadTimetable(BuildContext context) async {
  //   final pdf = PdfDocument();
  //   final table = pdf.addPage().addTable(columns: [
  //     TableCell(child: Text('Day')),
  //     ...List.generate(9, (index) => Text('${getTimeSlot(index)}')),
  //   ]);
  //   for (final day in timetable) {
  //     final row = table.addRow();
  //     row.cells[0] = TableCell(child: Text(weekdays[timetable.indexOf(day)]));
  //     for (int i = 1; i < day.length + 1; i++) {
  //       row.cells[i] = _buildPdfTimetableEntry(day[i - 1]);
  //     }
  //   }
  //   await pdf.save();
  //   // Show a success message or handle errors
  // }

  // PdfCell _buildPdfTimetableEntry(TimetableEntry entry) {
  //   if (entry.subject != null) {
  //     return PdfCell(
  //       child: Text(
  //         '${entry.subject!.name}\n(${entry.subject!.creditHours} CH)\n${entry.room}',
  //         style: const TextStyle(fontSize: 8),
  //       ),
  //     );
  //   } else {
  //     return const PdfCell(child: Text(''));
  //   }
  // }

  final weekdays = const ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
}
