import 'package:flutter/material.dart';


class AboutUsPage extends StatelessWidget {
  const AboutUsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About Abasyn University'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20.0),

              // University description
              const Text(
                'Abasyn University, Islamabad, Pakistan',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10.0),
              const Text(
                'Abasyn University is a leading institution of higher learning located in Islamabad, Pakistan. It offers a wide range of undergraduate and graduate programs across various disciplines. The university is committed to providing quality education and fostering innovation and entrepreneurship among its students.',
                textAlign: TextAlign.justify,
              ),
              const SizedBox(height: 20.0),

              // Contact information
              const Text(
                'Contact Information:',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10.0),
              const Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.location_on_outlined, color: Colors.orange,),
                  SizedBox(width: 10.0),
                  Flexible(
                    child: Text(
                      'Address: Abasyn University, Park Rd, near medkay hospital, '
                          'Meherban Colony Chatta Bakhtawar, Islamabad,'
                          ' Islamabad Capital Territory',
                      // Replace with actual address
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              const Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.phone, color: Colors.red,),

                  SizedBox(width: 10.0),
                  Flexible(
                    child: Text(
                      'Phone: (051) 8438320',
                      // Replace with actual phone number
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              const Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.email, color: Colors.green,),

                  SizedBox(width: 10.0),
                  Flexible(
                    child: Text(
                      'Email: financeoffice@abasynisb.edu.pk '
                          'Exam Office. Examination@abasynisb.edu.pk ',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

