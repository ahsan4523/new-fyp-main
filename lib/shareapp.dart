import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ShareAppScreen extends StatelessWidget {
  final String appLink = "https://yourappstorelink.com";

  const ShareAppScreen({super.key}); // Replace with your app link

  // Share app via WhatsApp
  void _shareOnWhatsApp() async {
    String url = "whatsapp://send?text=Check out this amazing app: $appLink";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // Share app via Gmail
  void _shareOnGmail() async {
    String url = "mailto:?subject=Check out this amazing app&body=$appLink";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // Share app via Facebook
  void _shareOnFacebook() async {
    String url = "https://www.facebook.com/sharer/sharer.php?u=$appLink";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // Share app via Bluetooth
  void _shareOnBluetooth() {
    // Implement Bluetooth sharing logic here
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Share This App'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              icon: const Icon(Icons.chat, size: 50, color: Colors.green),
              onPressed: () {
                _shareOnWhatsApp();
              },
            ),
            const Text('WhatsApp'),
            const SizedBox(height: 20),
            IconButton(
              icon: const Icon(Icons.mail, size: 50, color: Colors.red),
              onPressed: () {
                _shareOnGmail();
              },
            ),
            const Text('Gmail'),
            const SizedBox(height: 20),
            IconButton(
              icon: const Icon(Icons.facebook, size: 50, color: Colors.blue),
              onPressed: () {
                _shareOnFacebook();
              },
            ),
            const Text('Facebook'),
            const SizedBox(height: 20),
            IconButton(
              icon: const Icon(Icons.bluetooth, size: 50, color: Colors.teal),
              onPressed: () {
                _shareOnBluetooth();
              },
            ),
            const Text('Bluetooth'),
          ],
        ),
      ),
    );
  }
}
